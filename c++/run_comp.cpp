// basic one-compartment 7-conductance neuron model
// as in Prinz et al 2003
// written by Tim O'Leary
// 
#include <cmath>
#include <vector>
#include <typeinfo>
#include "mex.h"
#include "compartment.h"
#include "NaV.h"
#include "CaT.h"
#include "CaS.h"
#include "Ka.h"
#include "KCa.h"
#include "Kdr.h"
#include "Ih.h"
#include "leak.h"
// #include "chem_syn.h"


using namespace std;

// usage [output] = f([dt tstop res],[gbars], [inital_conditions])
// 1 -- voltage
// 2 -- calcium concentration 
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{
    double *output;
    double *simparams, *gmax, *initial_conditions, *neuron_params;
    int nits = 10, res = 1;
    double dt, tstop;
    double gbar_leak, gbar_na, gbar_kd, gbar_ka, gbar_kca, gbar_cas, gbar_cat, gbar_h;
    double e_leak = -50.0, e_na = 50.0, e_k = -80.0, e_h = -20.0, v0 = -60.0, Ca0 = 0.03, mNa = 0, mCaT = 0, mCaS = 0, mA = 0, mKCa = 0 , mKd = 0, mH = 0, hNa = 0, hCaT = 0, hCaS = 0, hA = 0;
    double tau_Ca, C, A, f, Ca_inside_cell, extCa;
    int state_dim = 0, full_state_dim = 0;
    double *state, *full_state;
    
    // parse inputs
    // prhs contains the arguments in the MATLAB wrapper
    simparams = mxGetPr(prhs[0]);
    gmax = mxGetPr(prhs[1]);
    initial_conditions = mxGetPr(prhs[2]);
    neuron_params = mxGetPr(prhs[3]);

    // simulation parameters
    dt = simparams[0];
    tstop = simparams[1];
    res = simparams[2];

    // initial conditions
    mNa = initial_conditions[0];
    mCaT = initial_conditions[1];
    mCaS = initial_conditions[2];
    mA = initial_conditions[3];
    mKCa = initial_conditions[4];
    mKd= initial_conditions[5];
    mH = initial_conditions[6];

    hNa = initial_conditions[7];
    hCaT = initial_conditions[8];
    hCaS = initial_conditions[9];
    hA = initial_conditions[10];

    v0 = initial_conditions[11];
    Ca0 = initial_conditions[12];


    // conductances
    gbar_na = gmax[0];
    gbar_cat = gmax[1];
    gbar_cas = gmax[2];
    gbar_ka = gmax[3];
    gbar_kca = gmax[4];
    gbar_kd = gmax[5];
    gbar_h = gmax[6];
    gbar_leak = gmax[7];

    // neuron parameters
    // neuron_parameters = [p.C p.A p.f p.extCa p.Ca0 p.tau_Ca e_Na e_K e_H e_leak];
    C = neuron_params[0];
    A = neuron_params[1];
    f = neuron_params[2];
    extCa = neuron_params[3];
    Ca_inside_cell = neuron_params[4];
    tau_Ca = neuron_params[5];


    e_na = neuron_params[6];
    e_k = neuron_params[7];
    e_h = neuron_params[8];
    e_leak = neuron_params[9];

    
    //make conductances
    leak gleak(gbar_leak,e_leak);
    NaV gna(gbar_na,e_na, mNa, hNa);
    CaT gcat(gbar_cat,120.0, mCaT ,hCaT); // this needs to be replaced..
    CaS gcas(gbar_cas,120.0, mCaS, hCaS); // with the actual Ca rev potential
    Ka gka(gbar_ka,e_k, mA ,hA);
    KCa gkca(gbar_kca,e_k, mKCa);
    Kdr gkdr(gbar_kd,e_k, mKd);
    Ih gh(gbar_h,e_h, mH);
    
    // mexPrintf("V_0 = %f\n", v0);
    // mexEvalString("drawnow;");

    //make compartment
    //compartment cell(v0,Ca0);
    compartment cell(v0, Ca0, C, A, f, extCa, Ca_inside_cell, tau_Ca, e_na, e_k, e_h, e_leak);
    cell.add_conductance(&gna);
    cell.add_conductance(&gcat);
    cell.add_conductance(&gcas);
    cell.add_conductance(&gka);
    cell.add_conductance(&gkca);
    cell.add_conductance(&gkdr);
    cell.add_conductance(&gh);
    cell.add_conductance(&gleak);
    
    nits = (int) floor(tstop/dt);

    full_state_dim = cell.get_state_dim();
    full_state = new double[full_state_dim];
    
    plhs[0] = mxCreateDoubleMatrix(full_state_dim, ((int)nits)/((int)res), mxREAL);

    output = mxGetPr(plhs[0]);


    //integration loop -- two Is refer to the integration step and the output step
    for(int I = 0, i=0; i<nits; i++)

    {
        (void) cell.integrate(dt);
        if (i%res == 0)
        {
            //cell.get_vcgs(state);
            cell.get_state(full_state);
            for(int j=0; j<full_state_dim; j++)
            {
                output[full_state_dim*I+j] = full_state[j];              
            }

            I++;
        }
    }

}
