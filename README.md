# neuron and network models

This repository contains neuron models implemented in various ways. This table summarizes the different models, their implementations, speeds and authors. 


| Model          | Paper reference | Implementation | Author | Speed | 
| -------          | ------- | ----------- | ------ | ---- | 
| 7-conductance point neuron [(neuron.m)](neuron-models/neuron.m)        | [Prinz 2003](http://jn.physiology.org/content/90/6/3998)  |  MATLAB        |  Srinivas Gorur-Shandilya    | 1.7X | 
|           | [O'Leary 2014](http://www.sciencedirect.com/science/article/pii/S089662731400292X)  |  C++     |  Timothy O'Leary, modified by Srinivas Gorur-Shandilya    | 64X |
|          |   |  Julia       |   Srinivas Gorur-Shandilya    | ~50X |


All code was measured on a Mac Pro from within MATLAB. Your speed may vary, but the relative speeds should be the same. The speed is reported as the ratio of simulated time to real time, for a fixed time step of 50 microseconds. 

# Usage 


`neuron` is a MATLAB class that uses a variety of implemenations under the hood. 

Generate a neuron object:

```
n = neuron;
```

Inspect default parameters:

```
n

n = 

neuron object with the following properties:

Parameter         Value
------------     --------
C_{m}            10 nF/mm^2
A                0.063 mm^2
Ca_{out}         3000 uM
Ca_{in}          0.05 uM
tau_{Ca}         200 ms
f                15 uM/nA
g_{Na}           183 mS/cm^2
g_{CaT}          2.3 mS/cm^2
g_{CaS}          2.7 mS/cm^2
g_{A}            24.6 mS/cm^2
g_{KCa}          98 mS/cm^2
g_{Kd}           61 mS/cm^2
g_{H}            1.01 mS/cm^2
g_{leak}         0.099 mS/cm^2

Underling implementation: 
-----------------------
SGS/Julia

```

Specify a integration timestep:

```matlab
n.dt = 50e-3; % in ms
```

Specify a end time: 

```
ABPD.t_end = 5000; % ms
```

View available implementations:

```
n.available_implementations

ans =

  1×3 cell array

    'SGS/Julia'    'TOL_Neuron_2013/C++'    'SGS/MATLAB'

```

Switch implementation:

```
n.implementation = 'TOL_Neuron_2013/C++';
```

Manipulate parameters of the neuron on the fly as it is being solved:

```
n.manipulate
```

![](https://gitlab.com/marderlab/neuron-network-models/uploads/8827de80b727e4311aeb2f3c6afc7834/test.mov.gif)


# Installation 

Clone this repo:

```
git clone https://gitlab.com/marderlab/neuron-network-models
``` 

## Julia

Make sure you have [Julia](https://julialang.org/) installed, and you've configured a alias so that `julia` can be called from the command line. 

## MATLAB 

1. Make sure you have a modern MATLAB version. I'm using `9.2.0.556344 (R2017a)`
2. Install `mexjulia`. The [original author](https://github.com/twadleigh/mexjulia) seems to have abandoned this project, so you should use my fork [here](https://github.com/sg-s/mexjulia)
3. [Follow the instructions](https://github.com/sg-s/mexjulia) to compile and install mexjulia
4. Install the following helper MATLAB toolboxes. The recommended way to install them is to use my MATLAB package manager:

```matlab
urlwrite('http://srinivas.gs/install.m','install.m'); 
install sg-s/srinivas.gs_mtools 
install sg-s/puppeteer
```

[`puppeteer`](https://github.com/sg-s/puppeteer) is used to manipulate parameters of the neuron model on the fly. 

# Contributing 

To add your own code/models to this repository, do the following:

Clone this repository 

```
git clone https://gitlab.com/marderlab/neuron-network-models
```


Make your own branch on your own computer

```
git branch "my-branch-name"
git checkout "my-branch-name"
```

Make your own changes, commit them, and **push your branch upstream**

Changes will then be merged into **master** 

# Tests

This repo includes a number of tests to make sure that the underlying implementations remain stable. If you are developing this repo, you are strongly encouraged to include this in a [pre-commit Git hook](https://git-scm.com/book/en/v2/Customizing-Git-Git-Hooks) so that you test your code before committing your code:

```bash

# check if tests pass on Julia typed code 
n_passed=$(julia julia-typed/test.jl | grep "passed" | wc -l)
if [ "$n_passed" -eq "2" ]; then
	echo "all tests passed";
else
   echo "at least one test failed! Fix this before you commit your code.";
   exit 666
fi

# automatic build number
git log master --pretty=oneline | wc -l > build_number
git add build_number
```
# License 

GPL v3, where applicable. 
