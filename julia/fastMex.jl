# this file serves as a bridge between MATLAB and Julia
# this file defines various functions that accept MATLAB arguments,
# and run various simulations in Julia and pass the results back to MATLAB

# simple 7-conductance neuron

function intNeuron7(neuron_params,reversal_potentials,max_conductances, initial_conditions)
    p = n7Parameters();
    s = simParameters();


    # update parameters
    p.cm = neuron_params[1];
    p.A = neuron_params[2];
    p.f = neuron_params[3];
    p.extCa = neuron_params[4];
    p.Ca0 = neuron_params[5];
    p.tau_Ca = neuron_params[6];
    p.conductances = max_conductances;
    p.reversal_potentials = reversal_potentials;
    p.initial_conditions = initial_conditions;


    s.t_end = neuron_params[7];
    s.dt = neuron_params[8];


    V, Ca, N = integrate(s,p);
    return N
end

function integrateDefaults()
    p = n7Parameters();
    s = simParameters();
    V, Ca, N = integrate(s,p);
    return N
end

# 7-conductance neuron with O'Leary style integral control 
function intHomeoNeuron7(neuron_params,reversal_potentials, initial_conditions)
    p = n7Parameters();
    s = simParameters();
    h = homeoControlParameters();


    # neuron parameters
    p.cm = neuron_params[1];
    p.A = neuron_params[2];
    p.f = neuron_params[3];
    p.extCa = neuron_params[4];
    p.Ca0 = neuron_params[5];
    p.tau_Ca = neuron_params[6];
    p.reversal_potentials = reversal_potentials;
    p.initial_conditions = initial_conditions[1:13];

    # simulation parameters
    s.t_end = neuron_params[7];
    s.dt = neuron_params[8];

    # homeostatic control parameters
    h.initial_conditions = initial_conditions[14:end];


    V, Ca, N = integrate(s,p,h);
    return N
end