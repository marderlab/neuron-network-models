
using Parameters

@with_kw type n7Parameters
    
    initial_conditions::Array{Float64,1} = [0., 0., 0., 0., 0., 0., 0., 0., 0., 0., 0., -70., 0.01]
    # define default parameters
    cm::Float64 = 10 # nF/mm^2
    A::Float64 = 0.0628 # sq mm
    p::Array{Int64,1} = [3, 3, 3, 3, 4, 4, 1, 1,] # exponents of activation variables
    extCa::Float64 = 3000 # in uM
    Ca0::Float64 = 0.05 # uM
    tau_Ca::Float64 = 200 # in milliseconds.
    f::Float64 = 1.496 #microMol/nA
    conductances::Array{Float64,1} = [1830., 23., 27., 246., 980., 610., 10.1, 0.99,]
    reversal_potentials::Array{Float64,1} = [30, 0,  0, -80, -80, -80, -20, -50,] # in mV

    # calculate capacitance (not used)
    C = cm*A; # nF

end

# define simulation parameters
@with_kw type simParameters
    dt::Float64 = 50e-3 # ms
    t_end::Int64 = 5000 # ms
end

# homeostatic control parameters
@with_kw type homeoControlParameters
    tau_g::Float64 = 100.0 # transcription timescale (ms)
    tau_mi::Array{Float64,1} = [666.7, 55555.6, 45454.5, 5000, 1250, 2000,    125000] # timescales+gains as in O'leary Neuron 2014
    target_Ca::Float64 = 7.0 # uM
    initial_conditions::Array{Float64,1} = rand(15)*.1; # 7 mRNA + 8 conductances 
end