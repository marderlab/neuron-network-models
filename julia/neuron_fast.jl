# 7-conductance neuron 
# based on Prinz et al 2003, Liu et al 1998 etc
# usage:
#
# include('neuron.jl')
# defaults = parameters()
# V, Ca, N = integrate(sim_params,neuron_params)
#


# integrate a simple 7-conductance neuron
function integrate(simp::simParameters, param::n7Parameters)
    # unpack parameters
    dt::Float64 = simp.dt;
    t_end::Float64 = simp.t_end;
    f::Float64 = param.f;
    A::Float64 = param.A;
    extCa::Float64 = param.extCa;
    reversal_potentials = param.reversal_potentials::Array{Float64,1};
    max_conductances = param.conductances::Array{Float64,1};
    p = param.p::Array{Int64,1};
    Ca0::Float64 = param.Ca0;
    cm::Float64 = param.cm;


    # preallocate some arrays
    nsteps::Int64 = floor(t_end/dt) + 1; 
    V::Array{Float64,1} = Array{Float64, 1}(nsteps); 
    Ca::Array{Float64,1} = Array{Float64, 1}(nsteps); 
    g = Array{Float64, 1}(8);
    zinf = Array{Float64, 1}(11);
    tz = Array{Float64, 1}(11);
    exp_dt_by_tau_Ca::Float64 = exp(-dt/param.tau_Ca);

    # (in)activation variables for the 8 channels (placeholders)
    h = ones(8);
    m = ones(8);

    # assemble inital_conditions 
    V[1] = param.initial_conditions[12];
    Ca[1] = param.initial_conditions[13];
    m[1:7] = param.initial_conditions[1:7];
    h[1:4] = param.initial_conditions[8:11];

    # placeholders
    m_inf = param.initial_conditions[1:7];
    h_inf = param.initial_conditions[8:11];
    tau_m = param.initial_conditions[1:7];
    tau_h = param.initial_conditions[8:11];
    
    R_by_zF = 500.0*(8.6174e-5);
    T = 10 + 273.15;
    RT_by_zF::Float64 = R_by_zF*T;
    fA = f*A;

    @inbounds for int_step = 2:nsteps

        V_prev = V[int_step-1];
        Ca_prev = Ca[int_step-1];

        # find m_inf, h_inf
        m_inf[1] = mNainf(V_prev);
        m_inf[2] = mCaTinf(V_prev);
        m_inf[3] = mCaSinf(V_prev);
        m_inf[4] = mAinf(V_prev);
        m_inf[5] = mKCainf(V_prev,Ca_prev);
        m_inf[6] = mKdinf(V_prev);
        m_inf[7] = mHinf(V_prev);

        h_inf[1] = hNainf(V_prev);
        h_inf[2] = hCaTinf(V_prev);
        h_inf[3] = hCaSinf(V_prev);
        h_inf[4] = hAinf(V_prev);

        # find tau_m, tau_h
        tau_m[1] = taumNa(V_prev);
        tau_m[2] = taumCaT(V_prev);
        tau_m[3] = taumCaS(V_prev);
        tau_m[4] = taumA(V_prev);
        tau_m[5] = taumKCa(V_prev);
        tau_m[6] = taumKd(V_prev);
        tau_m[7] = taumH(V_prev);

        # compute calcium reversal potentials 
        # exactly the same as O'Leary
        @fastmath reversal_potentials[2:3] = RT_by_zF*log(extCa/Ca_prev); # now in mV); % calcium reversal potential in mV, corrected for divalent Calcium

        # integrate m, h
        
        h[1] = h_inf[1] + (h[1] - h_inf[1])*exp(-(dt/tauhNa(V_prev)));
        h[2] = h_inf[2] + (h[2] - h_inf[2])*exp(-(dt/tauhCaT(V_prev)));
        h[3] = h_inf[3] + (h[3] - h_inf[3])*exp(-(dt/tauhCaS(V_prev)));
        h[4] = h_inf[4] + (h[4] - h_inf[4])*exp(-(dt/tauhA(V_prev)));
        
        for j = 1:7
            m[j] = m_inf[j] + (m[j] - m_inf[j])*exp(-(dt/tau_m[j]));
        end

        # compute g, g.*E using the new values of m, h
        for j = 1:8
            g[j] = max_conductances[j]*(m[j]^p[j])*h[j];
        end
        sigma_g = sum(g);

        # compute Calcium currents
        ca_current::Float64 = (g[2]+ g[3])*(V_prev - reversal_potentials[3]);
              
        # compute V_inf, tau_V using g <---- m(t), h(t) 
        V_inf::Float64 = (sum(g.*reversal_potentials))/sigma_g; # V_inf in mV
        tau_v::Float64 = cm/(sigma_g);

        # integrate V
        @fastmath V[int_step] = V_inf + (V_prev - V_inf)*exp(-(dt/tau_v)); # mV

        # compute Ca_inf
        cinf::Float64 = Ca0 - fA*ca_current; # microM
        @fastmath Ca[int_step] = cinf + (Ca_prev - cinf)*exp_dt_by_tau_Ca; # milliMol
    end 

    V = V[2:end]::Array{Float64,1};
    Ca = Ca[2:end]::Array{Float64,1};
    n = Array{Float64,2}(nsteps-1,13);
    n[end,1:7] = m[1:7];
    n[end,8:11] = h[1:4];
    n[:,12] = V;
    n[:,13] = Ca;
    return V, Ca, n
end 





