% neuron.m
% 7-conductance neuron model, as in Prinz et al. 2003
% this code was originally written in 2009 (!) by 
% Srinivas Gorur-Shandilya
% as part of the full STG model
% this version has been cannibalized form this
%
% usage:
% 
% ABPD = neuron;
% ABPD.dt = 50e-6;
% ABPD.t_end = 5;
% ABPD.initial_conditions = [0.0005    0.0046    0.0170    0.0116    0.0722    0.0106    0.0326 0.9621    0.9727    0.4293    0.8560  -65.6360    0.0296];
% tic
% [V, Ca, N] = ABPD.integrate;
% toc
%
% where 
% t_end is a scalar in seconds (length of simulation)
% dt is the time step of the solver
% conductances is a 8-element vector specifying conductances 
% following the same order as in Table 1 of Prinz et al. 
% (8th conductance is the leak)
% all conductances in mS/cm^2
%
% each neuron is 13-dimensional:
% N(:,1:7) handle the activation variables
% N(:,8:11) handle the inactivation variables (the others are always 1)
% N(:,12) is the membrane potential 
% N(:,13) is the calcium concentration 
 
% class-based version of neuron7

classdef neuron < handle & matlab.mixin.CustomDisplay

    properties
        dt = 50e-3 % ms
        t_end =  5000 % ms


        initial_conditions
        parameters
        units
        lb
        ub

        implementation
        handles

        % these parameters are more hard-wired, but can be changed if needed
        p = [3 3 3 3 4 4 1 1] % no units
        reversal_potentials = [30 0  0 -80 -80 -80 -20 -50]; % in mV

        voltage_clamp

    end


    properties (SetAccess = protected)
        available_implementations = {'SGS/Julia','TOL_Neuron_2013/C++','SGS/MATLAB','SGS/Julia-typed'};
    end

    methods (Access = protected)
        function displayScalarObject(self)
            url = 'https://gitlab.com/marderlab/neuron-network-models/';
            fprintf(['<a href="' url '">neuron</a> object with the following properties:\n\n']);


            fprintf('Parameter         Value\n')
            fprintf('------------     --------\n')
            f = fieldnames(self.parameters);
            for i = 1:length(f)
                padding_string_size = 17 - length(f{i});
                padding_string = repmat(' ',1,padding_string_size);
                v = [oval(self.parameters.(f{i})) ' ' self.units.(f{i})];
                this_string = [f{i} padding_string v '\n'];
                fprintf(this_string)
            end

            fprintf('\n')
            disp('Underling implementation: ')
            fprintf('-----------------------\n')
            disp(self.implementation)

            fprintf('\n')
            disp('Version (git hash): ')
            fprintf('-----------------------\n')
            disp(gitHash((which(mfilename))))

        end % end displayScalarObject
   end % end protected methods

    methods
        function self = neuron()

            self.initial_conditions = [0.0005 0.0046 0.0170 0.0116 0.0722    0.0106    0.0326 0.9621    0.9727    0.4293    0.8560  -65.6360    0.0296];


            % define parameters 
            p.C_m = 10;
            lb.C_m = 0;
            ub.C_m = 100;
            units.C_m = 'nF/mm^2';

            p.A = .0628;
            lb.A = 0;
            ub.A = 1;
            units.A = 'mm^2';

            p.Ca_out = 3000;
            lb.Ca_out = 0.03;
            ub.Ca_out = 5e3;
            units.Ca_out = 'uM';

            p.Ca_in = .05;
            lb.Ca_in = .01;
            ub.Ca_in = 1;
            units.Ca_in = 'uM';

            p.tau_Ca = 200;
            lb.tau_Ca = 1;
            ub.tau_Ca = 1e3;
            units.tau_Ca = 'ms';

            p.f = 1.496;
            lb.f = .1;
            ub.f = 10;
            units.f = 'uM/nA';

            p.gNa = 1830;
            lb.gNa = 0;
            ub.gNa = 1e4;
            units.gNa = 'uS/mm^2';

            p.gCaT = 23;
            lb.gCaT = 0;
            ub.gCaT = 1e4;
            units.gCaT = 'uS/mm^2';

            p.gCaS = 27;
            lb.gCaS = 0;
            ub.gCaS = 1e4;
            units.gCaS = 'uS/mm^2';

            p.gA = 246;
            lb.gA = 0;
            ub.gA = 1e4;
            units.gA = 'uS/mm^2';

            p.gKCa = 980;
            lb.gKCa = 0;
            ub.gKCa = 1e4;
            units.gKCa = 'uS/mm^2';

            p.gKd = 610;
            lb.gKd = 0;
            ub.gKd = 1e4;
            units.gKd = 'uS/mm^2';

            p.gH = 10.1;
            lb.gH = 0;
            ub.gH = 1e4;
            units.gH = 'uS/mm^2';

            p.gLeak = .993;
            lb.gLeak = 0;
            ub.gLeak = 1e4;
            units.gLeak = 'uS/mm^2';

            self.units = units;
            self.parameters = p;
            self.lb = lb;
            self.ub = ub;

            % set default implementation 
            self.implementation = self.available_implementations{1};

            % recompile binaries if needed
            self.recompile(false);
        end % end constructor


        function [V, Ca, n] = integrate(self)
            switch self.implementation
            case 'SGS/MATLAB'
                [V, Ca, n] = self.integrate_SGS_MATLAB;
            case 'TOL_Neuron_2013/C++'
                [V, Ca, n] = self.integrate_TOL_CPP;
            case 'SGS/Julia'
                [V, Ca, n] = self.integrate_SGS_JULIA;
            case 'SGS/Julia-typed'
                [V, Ca, n] = self.integrate_JULIA_TYPED;
            otherwise
                error('Unknown implementation')
            end
                   
        end

        function [V, Ca, n] = integrate_SGS_MATLAB(self)

            % assemble parameters
            p = self.p(:); %  exponents of m
            parameter_names = fieldnames(self.parameters);
            P = self.parameters;
            max_conductances = [P.gNa; P.gCaT; P.gCaS; P.gA; P.gKCa; P.gKd; P.gH; P.gLeak];
            reversal_potentials = self.reversal_potentials(:);

            % assemble inital_conditions 
            nsteps = floor(self.t_end/self.dt) + 1; % to agree with TO'L
            if isempty(self.voltage_clamp)
                n = zeros(nsteps,13);
                n(1,:) = self.initial_conditions;
            else
                % cell is being voltage clamped 
                n = zeros(nsteps,14); % use this extra channel to report the currents
                n(1,1:13) = self.initial_conditions(1:13);
                n(1,12) = self.voltage_clamp(1);
            end
            

            % (in)activation variables for the 8 channels (placeholders)
            h = ones(8,1);
            m = ones(8,1);

            zinf = zeros(1,11);
            exp_dt_by_tau_Ca = exp(-self.dt/self.parameters.tau_Ca);
            tz = zeros(1,11);

            dt = self.dt;

            R_by_zF = 500.0*(8.6174e-005);
            T = 10 + 273.15;
            fA = self.parameters.f*self.parameters.A;


            for step = 2:size(n,1)

                prev_step = step - 1;
                V_prev = n(prev_step,12);
                Ca_prev = n(prev_step,13);

                % find m_inf, h_inf
                zinf(1:7) = self.find_minf(V_prev,Ca_prev);
                zinf(8:11) = self.find_hinf(V_prev);

                % find tau_m, tau_h
                tz(1:7) = self.find_tm(V_prev);
                tz(8:11) = self.find_th(V_prev);

                 % compute calcium reversal potentials 
                % exactly the same as O'Leary
                reversal_potentials(2:3) = R_by_zF*T*log(self.parameters.Ca_out/Ca_prev); % now in mV); % calcium reversal potential in mV, corrected for divalent Calcium

                % integrate m, h
                n(step,1:11) = zinf + (n(prev_step,1:11) - zinf).*exp(-(dt./tz));

                % compute g, g.*E using the new values of m, h
                m(1:7) = n(step,1:7);
                h(1:4) = n(step,8:11);
                g = max_conductances.*(m.^p).*h; 
                sigma_g = sum(g);


                % compute Calcium currents
                ca_current = g(2)*(V_prev - reversal_potentials(2)) + g(3)*(V_prev - reversal_potentials(3));
                    

                if isempty(self.voltage_clamp)
                    % compute V_inf, tau_V using g <---- m(t), h(t) 
                    V_inf = (sum(g.*reversal_potentials))/sigma_g;% V_inf in mV
                    tau_v = self.parameters.C_m/(sigma_g);


                    % integrate V
                    n(step,12) = V_inf + (V_prev - V_inf)*exp(-(dt/tau_v)); % mV
                else
                    % the cell is being voltage clamped
                    n(step,12) = self.voltage_clamp(step-1);
                    n(step,14) = self.parameters.A*(self.voltage_clamp(step-1)*sigma_g  - (sum(g.*reversal_potentials))); % nA
                    
                end

                % compute Ca_inf
                cinf = self.parameters.Ca_in - fA*ca_current; % microM

                n(step,13) = cinf + (Ca_prev - cinf)*exp_dt_by_tau_Ca; %milliMol

            end 

            % discard step 1
            n = n(2:end,:);

            V = n(:,12)';
            Ca = n(:,13)';

            % update initial conditions
            self.initial_conditions = n(end,:);

        end % end integrate SGS/MATLAB


        function [V, Ca, N] = integrate_TOL_CPP(self)
            % parameters = self.parameters;
            % v2struct(parameters)

            % % shim
            % p = self.parameters;
            % gS = p.conductances;
            e_leak = self.reversal_potentials(8);
            e_Na = self.reversal_potentials(1);
            e_K = self.reversal_potentials(4);
            e_H = self.reversal_potentials(7);

            % neuron_parameters = [p.C p.A p.f p.extCa p.Ca0 p.tau_Ca e_Na e_K e_H e_leak];


            % assemble parameters
            max_conductances = self.parameters(~cellfun(@isempty,cellfun(@(x) strfind(x,'g_'),self.parameter_names,'UniformOutput',false)));
            max_conductances = max_conductances(:)';
            for i = 1:length(self.parameter_names)

                feval(@()assignin('caller',self.legal_parameter_names{i}, self.parameters(i)));
            end



            neuron_parameters = [C_m*A A f Ca_out Ca_in tau_Ca e_Na e_K e_H e_leak];

            % run using Tim O'Leary's code
            N = run_comp([self.dt self.t_end 1],max_conductances,self.initial_conditions, neuron_parameters);
            
            % re-arrange N
            new_N = N;
            new_N(12:13,:) = N(1:2,:);
            new_N(1,:) = N(3,:); % mNa
            new_N(8,:) = N(4,:); % hNa
            new_N(2,:) = N(5,:); % mCaT
            new_N(9,:) = N(6,:); % hCaT
            new_N(3,:) = N(7,:); % mCaS
            new_N(10,:) = N(8,:); % hCaS
            new_N(4,:) = N(9,:); % mA
            new_N(11,:) = N(10,:); % hA
            new_N(5,:) = N(11,:); % mKCa
            new_N(6,:) = N(12,:); % mKd
            new_N(7,:) = N(13,:); % mH

            N = new_N;

            % update initial conditions
            self.initial_conditions = N(:,end);

            % export
            V = N(12,:);
            Ca = N(13,:);
        

        end % end integrate C++/TOL

        function [V, Ca, N] = integrate_JULIA_TYPED(self)

            % assemble parameters
            p = self.p(:)'; %  exponents of m
            P = self.parameters;
            max_conductances = [P.gNa; P.gCaT; P.gCaS; P.gA; P.gKCa; P.gKd; P.gH; P.gLeak];
            neuron_parameters = [P.C_m P.A P.f P.Ca_out P.Ca_in P.tau_Ca self.t_end self.dt];

            % run using Julia
            N = jl.call('intComp1',neuron_parameters(:), self.initial_conditions(:));
            N = struct2mat(N);
            V = N(:,1);
            Ca = N(:,2);

            % update initial conditions
            self.initial_conditions(12) = V(end);
            self.initial_conditions(13) = Ca(end);
        end

        function [V, Ca, N] = integrate_SGS_JULIA(self)

            % assemble parameters
            p = self.p(:)'; %  exponents of m
            P = self.parameters;
            max_conductances = [P.gNa; P.gCaT; P.gCaS; P.gA; P.gKCa; P.gKd; P.gH; P.gLeak];
            neuron_parameters = [P.C_m P.A P.f P.Ca_out P.Ca_in P.tau_Ca self.t_end self.dt];

            % run using Julia
            % N = jl.call('integrateDefaults');
            N = jl.call('intNeuron7',neuron_parameters(:),self.reversal_potentials(:), max_conductances(:), self.initial_conditions(:));

            % export
            V = N(:,12)';
            Ca = N(:,13)';

            % update initial conditions 
            self.initial_conditions = N(end,:);
        

        end % end integrate Julia code

        function recompile(self,force)
            if ~exist('force','var')
                force = true;
            end

            p = fileparts(which(mfilename));

            % build platform-specific binary of Tim's C++ code
            if ismac 
                allfiles = dir([p '/c++/run_comp.mexmaci64']);
                if isempty(allfiles) || force
                    mex([p '/c++/run_comp.cpp'])
                end
            elseif ispc 
                allfiles = dir([p '\c++\run_comp.mexw64']);
                if isempty(allfiles) || force
                    mex([p 'c++\run_comp.cpp'])
                end
            else
                allfiles = dir([p '/c++/run_comp.mexa64']);
                if isempty(allfiles) || force
                    mex([p '/c++/run_comp.cpp'])
                end
            end

            % add fast Julia code
            pj = joinPath(p,'julia');
            jl.include(joinPath(pj,'param_types.jl')) 
            jl.include(joinPath(pj,'liu_gating_functions.jl'))
            jl.include(joinPath(pj,'neuron_fast.jl'))
            jl.include(joinPath(pj,'fastMex.jl'))

            % add typed Julia code 
            pj = joinPath(p,'julia-typed');
            jl.include(joinPath(pj,'conductances_typed.jl')) 
            jl.include(joinPath(pj,'neuron_typed.jl'))
            jl.include(joinPath(pj,'typedMex.jl'))

        end % end recompile 


        function plotVoltageDependence(self)
            figure('outerposition',[200 100 1800 701],'PaperUnits','points','PaperSize',[1800 701]); hold on
            V = linspace(-90,70,1e3);
            Ca = 3e-3;

            M = zeros(length(V),7);
            tau_M = zeros(length(V),7);
            H = zeros(length(V),4);
            tau_H = zeros(length(V),4);
            for i = 1:length(V)
                M(i,:) = find_minf(V(i),Ca);
                tau_M(i,:) = find_tm(V(i));
                H(i,:) = find_hinf(V(i));
                tau_H(i,:) = find_th(V(i));
            end

            L = {'Na','CaT','CaS','A','KCa','Kd','H'};

            for i = 1:7
                subplot(2,7,i); hold on
                plot(V,M(:,i),'k')
                try
                    plot(V,H(:,i),'r')
                catch
                    plot(V,1+0*V,'r')
                end
                legend({'m_{\infty}','h_{\infty}'},'Location','southeast')
                xlabel('V_m (mV)')
                title(L{i})

                subplot(2,7,i+7); hold on
                plot(V,tau_M(:,i),'k')
                try
                    plot(V,tau_H(:,i),'r')
                catch
                    plot(V,1+0*V,'r')
                end
                legend({'tau_{M}','tau_H'},'Location','northeast')
                xlabel('V_m (mV)')
                set(gca,'YLim',[.1 1e4],'YScale','log')
                title(L{i})
            end

            prettyFig();
        end 

        function minf = find_minf(self,V,Ca)
            minf = zeros(7,1);
            minf(1) = 1.0/(1.0 + exp((V + 25.5)/(-5.29))); 
            minf(2) = 1.0/(1.0 + exp((V + 27.1)/(-7.20)));
            minf(3) = 1.0/(1.0 + exp((V + 33.0)/(-8.10)));
            minf(4) = 1.0/(1.0 + exp((V + 27.2)/(-8.70)));
            minf(5) = (Ca/(Ca+3.0))/(1.0+exp((V + 28.3)/-12.6)); 
            minf(6) = 1.0/(1.0 + exp((V + 12.3)/(-11.80)));

            % minf(7) = 1.0/(1.0 + exp((V + 75.0)/(5.5)));

            % using Tim's version
            minf(7) = 1.0/(1.0+exp((V + 70.0)/6.0));
        end

        function hinf = find_hinf(self,v);
            hinf = zeros(1,4);
            hinf(1) = 1.0/(1.0+exp((v + 48.9)/5.18));
            hinf(2) = 1.0/(1.0+exp((v + 32.1)/5.5));
            hinf(3) = 1.0/(1.0+exp((v + 60)/6.2));
            hinf(4) = 1.0/(1.0+exp((v + 56.9)/4.9));
        end



        function taum = find_tm(self,v)
            taum = zeros(1,7);

            % using Tim's equation instead
            taum(1) = 1.320 - (1.26/(1 + exp((v + 120)/(-25))));
            taum(2) = 21.7 - (21.3/(1 + exp((v + 68.1)/(-20.5))));
            taum(3) = 1.4 + (7.0/(exp((v + 27.0)/(10.0)) + exp((v + 70.0)/(-13.0))));
            taum(4) = 11.6 - (10.4/(1 + exp((v + 32.9)/(-15.2))));
            taum(5) = 90.3 - 75.1/(1.0+exp((v + 46.0)/-22.7));
            taum(6) = 7.2 - 6.4/(1.0+exp((v + 28.3)/-19.2));
            taum(7) = (272.0 + 1499.0/(1.0+exp((v + 42.2)/-8.73)));
            
           
        end


        function tau_h = find_th(self,V)
            tau_h = zeros(4,1);

            % using Tim's equations instead
            tau_h(1) = (0.67/(1.0 + exp((V + 62.9)/(-10))))*((1.5)+(1.0/(1.0+(exp((V+34.9)/(3.6)))))); 
            tau_h(2) = 105.0 - (89.8/(1.0 + exp((V + 55.0)/(-16.9))));
            tau_h(3) = 60 + (150/(exp((V + 55)/(9.0)) + exp((V + 65.0)/(-16.0))));
            tau_h(4) = 38.6 - (29.2/(1 + exp((V + 38.9)/(-26.5))));

            
        end



        function [] = benchmark(self)
            all_time_steps = linspace(10,100,10)/1e3;
            ref_time_step = all_time_steps(1);

            figure('outerposition',[200 200 700 700],'PaperUnits','points','PaperSize',[700 700]); hold on
            set(gca,'YScale','log')
            xlabel('Time step (\mu s)')

            ic = self.initial_conditions;

            c = lines(4);

            for i = 1:2
                r2 = NaN*all_time_steps;
                sim_time = r2*NaN;

                l2 = plot(1e3*all_time_steps,sim_time,'+-','Color',c(i,:));
                ylabel('Speed (X)')

                self.implementation = self.available_implementations{i};
                
                
                self.t_end = 1e3;
                self.dt = all_time_steps(1);
                tic
                ref_V = self.integrate;
                t = toc;
                r2(1) = 1;
                sim_time(1) = 1/t;

                for j = 2:length(r2)
                    self.initial_conditions = ic;
                    self.dt = all_time_steps(j);
                    tic
                    this_V = self.integrate;
                    t = toc;
                    fullT = ref_time_step:ref_time_step:self.t_end;
                    this_V = interp1(self.dt*(1:length(this_V)),this_V,fullT);
                    r2(j) = rsquare(this_V,ref_V);
                    sim_time(j) = 1/t;
                    l2.YData = sim_time;
                    drawnow;
                end

            end
            legend(self.available_implementations{1:2})
            prettyFig

        end % end varyTimeStep



        function checkBounds(self)
            assert(~any(self.lb >= self.ub),'At least one lower bound is greater than a upper bound');
            assert(all(self.parameters <= self.ub) && all(self.parameters >= self.lb),'At least one parameter out of bounds');
            max_conductances = self.parameters(~cellfun(@isempty,cellfun(@(x) strfind(x,'g_'),self.parameter_names,'UniformOutput',false)));
            assert(min(max_conductances) >= 0,'Conductances cannot be < 0');
        end

        function manipulate(self)


            % create a window to show the voltage traces 
            self.handles.time_series_fig = figure('position',[1000 250 1400 900], 'Toolbar','none','Menubar','none','NumberTitle','off','IntegerHandle','off','Name',['manipulate[neuron]']);
            self.handles.ax(1) = subplot(2,1,1); hold on
            self.handles.ax(2) = subplot(2,1,2); hold on
            xlabel(self.handles.ax(2),'Time (ms)')
            ylabel(self.handles.ax(2),'Ca (\mu M)')
            ylabel(self.handles.ax(1),'V (mV)')

            % spawn a puppeteer object
            pp = puppeteer(self.parameters,self.lb,self.ub);

            % configure 
            attachFigure(pp,self.handles.time_series_fig)

            % while the window is valid, simulate in 2-second blocks and show it
            window_size = 1000;
            self.handles.ax(1).XLim = [0 window_size];
            self.handles.ax(2).XLim = [0 window_size];
            dt = 100e-3;
            self.dt = dt;

            buffer_size = 10; 
            buffer_chunk_size = floor(buffer_size/dt);
            self.t_end = buffer_size;

            T = (dt:dt:window_size);
            V = NaN*(dt:dt:window_size);
            C = NaN*(dt:dt:window_size);

            self.handles.V_trace = plot(self.handles.ax(1),T,V,'k','LineWidth',2);
            self.handles.C_trace = plot(self.handles.ax(2),T,V,'k','LineWidth',2);

            prettyFig();
            

            % make sure it runs realtime 
            if isfield(self.handles,'time_series_fig')
                while isvalid(self.handles.time_series_fig)
                    tic

                    % read new parameters from puppeteer 
                    self.parameters = pp.parameters;

                    [this_V, this_C] = self.integrate;

                    % throw out the first bit 
                    V(1:buffer_chunk_size) = [];
                    C(1:buffer_chunk_size) = [];
                    
                    % append the new data to the end
                    V = [V this_V];
                    C = [C this_C];
    
                    self.handles.V_trace.XData = T;
                    self.handles.V_trace.YData = V;

                    self.handles.C_trace.XData = T;
                    self.handles.C_trace.YData = C;

                    drawnow limitrate
                    t = toc;
                    t = t;
                    pause_time = (buffer_size/1e3) - t; 
                    if pause_time > 0
                        pause(pause_time*3)
                    end
                end
            end
        end % end manipulate 

    end % end methods 

end % end classdef 



