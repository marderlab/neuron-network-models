# neuron.jl
# specifies types and function for a single compartment neuron


# create a container for neurons
mutable struct Cell
    # dynamic variables
    V::Float64                              # mV
    Ca::Float64                            # μM

    # conductances
    conductance::Array{Conductance,1}

    # parameters
    area::Float64 # mm^2
    Cm::Float64 # nF/mm^2
    extCa::Float64  # μM
    τCa::Float64  # ms
    Ca0::Float64   # μM
    f::Float64 # μM/nA
    RT_by_zF::Float64 # to compute E_Ca

    # housekeeping
    Ca_channels::Array{Int64,1} # marker for which channels are Calcium channels
    E::Array{Float64,1}

    voltage_clamp::Array{Float64,1}

    # constructor
    Cell() = new(-70.0,0.05,Array{Conductance,1}(),0.0628,10.,3000.,200.,0.05,1.496,12.20008405,Array{Int64,1}(),Array{Float64,1}(),Array{Float64,1}())
    Cell(v,ca,g,a,cm,extCa,τCa,Ca0,f,rt,Ca_channels,E,voltage_clamp) = new(v,ca,g,a,cm,extCa,τCa,Ca0,f,rt,Ca_channels,E,voltage_clamp)
end

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~ begin function definitions ~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

# add cell to cells
function add!(cells::Array{Cell,1},cell::Cell)
    push!(cells,cell)
end

function add!(synapses::Array{Synapse,1},syn::Synapse)
    push!(synapses,syn)
end

# add conductance to cell
function add!(cell::Cell, g::Conductance)
    if length(string(typeof(g))) > 1
        if string(typeof(g))[1:2] == "Ca"
            push!(cell.Ca_channels,length(cell.conductance)+1)
        end
    end
    push!(cell.conductance,g)
    push!(cell.E,g.E)
end


function integrate(cells::Array{Cell,1},synapses::Array{Synapse,1},dt::Float64,tmax::Float64)

    # make some placeholders and arrays 
    nsteps  = Int64(tmax/dt)
    V_out       = zeros(nsteps,length(cells))
    Ca_out      = zeros(nsteps,length(cells))
    I_clamp     = zeros(nsteps,length(cells))

    clamp_this = Array{Bool,1}(length(cells))
    @inbounds for ii in 1:length(cells)
        if ~isempty(cells[ii].voltage_clamp)
            clamp_this[ii] = true
        end
    end

    @inbounds for ns = 2:nsteps

        # first integrate the conductances for all the cells 
        @inbounds for ii in 1:length(cells)
            cell = cells[ii]
            V = cell.V;
            Ca = cell.Ca;

            # integrate m, h for every channel 
            @inbounds for i = 1:length(cell.conductance)
                integrate(cell.conductance[i], V, Ca, dt)
            end

            # integrate the synapses
            for j in 1:length(synapses)
                if synapses[j].n_post == ii
                    integrate(synapses[j],V,cells[synapses[j].n_pre].V,dt)
                end
            end
        end # end all cells loop 1 (conductances)

        # now integrate the voltage and the calcium for all cells 
        @inbounds for ii in 1:length(cells)
            cell = cells[ii]
            V = cell.V;
            Ca = cell.Ca;
            g = Array{Float64,1}(length(cell.conductance)) # this needs to be redefined here because different cells can have different numbers of conductances 
            sigma_gE = 0;

            @inbounds for qq in 1:length(cell.conductance)
                g[qq] = getConductance(cell.conductance[qq])
                sigma_gE += g[qq]*cell.E[qq]
            end

            sigma_g = sum(g);

            # synapses 
            Isyn::Float64 = 0.0
            for j in 1:length(synapses)
                if synapses[j].n_post == ii
                    Isyn += getConductance(synapses[j],V,cells[synapses[j].n_pre].V)
                end
            end

            # compute Calcium currents
            E_Ca = cell.RT_by_zF*log(cell.extCa/Ca);
            cell.E[cell.Ca_channels] = E_Ca;
            Ca_current = (V - E_Ca)*sum(g[cell.Ca_channels]);



            # integrate V
            if clamp_this[ii]
                cell.V = cell.voltage_clamp[ns-1]
                # calculate I_clamp
                I_clamp[ns,ii] = cell.area*(cell.V*sigma_g - sigma_gE) - Isyn

            else
                # compute V_inf, tau_V using g <---- m(t), h(t)
                V_inf::Float64 = (sigma_gE+Isyn/cell.area)/sigma_g; # V_inf in mV
                tau_V::Float64 = cell.Cm/(sigma_g);
                cell.V = V_inf + (V - V_inf)*exp(-(dt/tau_V)); # mV
            end

            # compute Cal_inf
            cinf::Float64 = cell.Ca0 - cell.f*cell.area*Ca_current; # microM
            cell.Ca = cinf + (Ca - cinf)*exp(-dt/cell.τCa); # microM
            

            # store this
            V_out[ns,ii] = cell.V;
            Ca_out[ns,ii] = cell.Ca;
        end # end all cells loop 2 (voltage and calcium)
    end # end all steps 
    return V_out, Ca_out, I_clamp
end # end integrate function 
