# this file serves as a bridge between MATLAB and Julia
# this file defines various functions that accept MATLAB arguments,
# and run various simulations in Julia and pass the results back to MATLAB

# simple 7-conductance neuron

function intComp1(neuron_params, initial_conditions)

    # test a basic cell, and see if it spikes normally
    cells       = Array{Cell,1}();
    synapses    = Array{Synapse,1}();

    # Cm = Float64(neuron_params[1]);
    # A = Float64(neuron_params[2]);
    # f = Float64(neuron_params[3]);
    # extCa = Float64(neuron_params[4]);
    # Ca0 = Float64(neuron_params[5]);
    # tau_Ca = Float64(neuron_params[6]);

    Cm = 10.;
    A = 0.0628;
    f = 1.495;
    extCa =3000.;
    Ca0 = 0.05;
    tau_Ca = 200.;


    g = [NaV_Liu(1830.), CaT_Liu(23.), CaS_Liu(27.), A_Liu(246.), KCa_Liu(980.), Kd_Liu(610.), H_Liu(10.), Leak(.99)];
    Ca_channels = [2,3];
    E = [30.0,130.0,130.0,-80.0,-80.0,-80.0,-20.0,-50.0];

    comp = Cell(-70.0,.05,g,A,Cm,extCa,tau_Ca,Ca0,f,12.20008405,Ca_channels,E,Array{Float64,1}());
    

    # update parameters

    # comp.conductances = max_conductances;
    # comp.reversal_potentials = reversal_potentials;
    # comp.initial_conditions = initial_conditions;

    # # set the initial conditions of all these channels for reproducibility
    # for ii = 1:4
    #     comp.conductance[ii].h = 0.01;
    # end
    # for ii = 1:7
    #     comp.conductance[ii].m = 0.01;
    # end

    cells = [comp];
    N = integrate(cells, synapses, 50e-3, Float64(5e3));
    return N
end
