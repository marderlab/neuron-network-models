

workspace(); gc();
include("conductances_typed.jl")
include("neuron_typed.jl")
# using Plots;
# pyplot()

# test a basic cell, and see if it spikes normally
cells       = Array{Cell,1}();
synapses    = Array{Synapse,1}();

AB          = Cell()
add!(AB,NaV_Liu(1830.));     # ḡ in μS/mm^2
add!(AB,CaT_Liu(23.));
add!(AB,CaS_Liu(27.));
add!(AB,A_Liu(246.));
add!(AB,KCa_Liu(980.));
add!(AB,Kd_Liu(610.));
add!(AB,H_Liu(10.));
add!(AB,Leak(.99));

# set the initial conditions of all these channels for reproducibility
for i = 1:4
  AB.conductance[i].h = 0;
end
for i = 1:7
  AB.conductance[i].m = 0;
end

push!(cells,AB)
V, Ca, I_clamp = integrate(cells, synapses, 50e-3, 5e3);

if hash(V) == 0x1fc299345f6da5ba
    print("test 1 passed\n")
else
    print("test 1 failed\n")
end


## Voltage clamp test
# Now, we voltage clamp a cell with only a potassium conductance

cells       = Array{Cell,1}();
synapses    = Array{Synapse,1}();

AB          = Cell()
add!(AB,Kd_Liu(610.));
add!(AB,Leak(.99));

AB.conductance[1].m = 0;

# test voltage clamp
V_clamp = zeros(100000) - 50;
V_clamp[10000:20000] = 0;
V_clamp[20000:30000] = 50;
AB.voltage_clamp = V_clamp;

push!(cells,AB)
V, Ca, I_clamp = integrate(cells, synapses, 50e-3, 5e3);

if hash(I_clamp) == 0x4a634a007b0f9ade
    print("test 2 passed\n")
else
    print("test 2 failed\n")
end
