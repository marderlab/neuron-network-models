# conductances.jl
# defines types and functions for various conductances


abstract type Conductance
    # all conductances now inherit from this
end

# helper functions
boltz(V::Float64,A::Float64,B::Float64) = 1/(1 + exp((V+A)/B))
tauX(V::Float64,A::Float64,B::Float64,D::Float64,E::Float64) = A - B/(1+exp((V+D)/E))



# leak conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct Leak <: Conductance
    g::Float64      # maximal conductance in μS/mm^2
    E::Float64

    # constructor
    Leak(x) = new(x,-50.0);
end

function integrate(channel::Leak,V::Float64,Ca::Float64,dt::Float64)
    return Void()
end

function getConductance(channel::Leak)
    return channel.g
end

# fast sodium conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct NaV_Liu <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    NaV_Liu(x::Float64)  = new(x,0.,0.,30.)
end

function integrate(channel::NaV_Liu,V::Float64,Ca::Float64,dt::Float64)
    minf = boltz(V,25.5,-5.29);
    hinf = boltz(V,48.9,5.18);
    tauh = (0.67/(1+exp((V+62.9)/-10.0)))*(1.5 + 1/(1+exp((V+34.9)/3.6)));

    channel.m = minf + (channel.m - minf)*exp(-dt/tauX(V,1.32,1.26,120.,-25.));
    channel.h = hinf + (channel.h - hinf)*exp(-dt/tauh);
    return Void()
end

function getConductance(channel::NaV_Liu)
    return channel.g*(channel.m^3)*channel.h;
end

# transient calcium conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct CaT_Liu <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    CaT_Liu(x::Float64)  = new(x,0.0,0.0,130.)
end

function integrate(channel::CaT_Liu,V::Float64,Ca::Float64,dt::Float64)
    minf = boltz(V,27.1,-7.2);
    hinf = boltz(V,32.1,5.5);

    channel.m = minf + (channel.m - minf)*exp(-dt/tauX(V,21.7,21.3,68.1,-20.5));
    channel.h = hinf + (channel.h - hinf)*exp(-dt/tauX(V,105.,89.8,55.,-16.9));
    return Void()
end

function getConductance(channel::CaT_Liu)
    channel.g*(channel.m^3)*channel.h;
end


# slow calcium conductance ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct CaS_Liu <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    CaS_Liu(x::Float64)  = new(x,0.,0.,130.)
end

function integrate(channel::CaS_Liu,V::Float64,Ca::Float64,dt::Float64)
    minf = boltz(V,33.,-8.1);
    hinf = boltz(V,60.,6.2);
    taum = 1.4 + (7/((exp((V+27)/10))+(exp((V+70)/-13))));
    tauh = 60 + (150/((exp((V+55)/9))+(exp((V+65)/-16))));

    channel.m = minf + (channel.m - minf)*exp(-dt/taum);
    channel.h = hinf + (channel.h - hinf)*exp(-dt/tauh);

    return Void()
end

function getConductance(channel::CaS_Liu)
    channel.g*(channel.m^3)*channel.h;
end

# A current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct A_Liu <: Conductance
    g::Float64
    m::Float64
    h::Float64
    E::Float64

    # constructor
    A_Liu(x::Float64)  = new(x,0.,0.,-80.)
end

function integrate(channel::A_Liu,V::Float64,Ca::Float64,dt::Float64)
    channel.m = boltz(V,27.2,-8.7) + (channel.m - boltz(V,27.2,-8.7))*exp(-dt/tauX(V,11.6,10.4,32.9,-15.2));
    channel.h = boltz(V,56.9,4.9) + (channel.h - boltz(V,56.9,4.9))*exp(-dt/tauX(V,38.6,29.2,38.9,-26.5));

     return Void()
end

function getConductance(channel::A_Liu)
    channel.g*(channel.m^3)*channel.h;
end

# KCa current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct KCa_Liu <: Conductance
    g::Float64
    m::Float64
    E::Float64

    # constructor
    KCa_Liu(x::Float64)  = new(x,0.,-80.)
end

function integrate(channel::KCa_Liu,V::Float64,Ca::Float64,dt::Float64)
    minf = (Ca/(Ca+3))*(1/(1+exp((V+28.3)/-12.6)));

    channel.m = minf + (channel.m - minf)*exp(-dt/tauX(V,90.3,75.1,46.,-22.7));

    return Void()
end

function getConductance(channel::KCa_Liu)
    channel.g*(channel.m^4);
end


# Kd current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct Kd_Liu <: Conductance
    g::Float64
    m::Float64
    E::Float64

    # constructor
    Kd_Liu(x::Float64)  = new(x,0.,-80.)
end

function integrate(channel::Kd_Liu,V::Float64,Ca::Float64,dt::Float64)
    minf = boltz(V,12.3,-11.8);

    channel.m = minf + (channel.m - minf)*exp(-dt/tauX(V,7.2,6.4,28.3,-19.2));

    return Void()
end

function getConductance(channel::Kd_Liu)
    channel.g*(channel.m^4);
end

# H current  ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
mutable struct H_Liu <: Conductance
    g::Float64
    m::Float64
    E::Float64

    # constructor
    H_Liu(x::Float64)  = new(x,0.,-20.)
end

function integrate(channel::H_Liu,V::Float64,Ca::Float64,dt::Float64)
    channel.m = boltz(V,70.0,6.0) + (channel.m - boltz(V,70.0,6.0))*exp(-dt/((272.0 + 1499.0/(1.0+exp((V + 42.2)/-8.73)))));

    return Void()
end

function getConductance(channel::H_Liu)
    channel.g*channel.m;
end

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## ~~~~~~~~~~~~~~~~~~~~~~~~~~~~ Synapses ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

abstract type Synapse end

# Electric synapse (gap junction) ~~~~~~~~~~~~~~~~~~~~~~~
mutable struct Gap <: Synapse
    g::Float64  # μS
    n_pre::Int64
    n_post::Int64

    Gap(x::Float64,y::Int64,z::Int64) = new(x,y,z)
end

function integrate(syn::Gap,Vpost::Float64,Vpre::Float64,dt::Float64)
    return Void
end

function getConductance(syn::Gap,Vpost::Float64,Vpre::Float64)
    -syn.g * (Vpost - Vpre)   # nA
end

# Chemical synapse (glutamatergic, graded) ~~~~~~~~~~~~~~
mutable struct Glut <: Synapse
    g::Float64
    s::Float64
    E::Float64

    n_pre::Int64
    n_post::Int64

    # constructor
    Glut(x::Float64,y::Int64,z::Int64) = new(x,0.,-70.0,y,z)
end

function integrate(syn::Glut,Vpost::Float64,Vpre::Float64,dt::Float64)
    sinf    = boltz(Vpre,-35.,-5.)
    syn.s   = sinf + (syn.s - sinf)*exp(-dt/(40.*(1-sinf)))
    return Void()
end

function getConductance(syn::Glut)
    syn.s * (Vpost - syn.E)
end

# Chemical synapse (cholinergic, graded) ~~~~~~~~~~~~~~~~
mutable struct Chol <: Synapse
    g::Float64
    s::Float64
    E::Float64

    n_pre::Int64
    n_post::Int64

    # constructor
    Chol(x::Float64) = new(x,0.,-80.0)
end

function integrate(syn::Chol,Vpost::Float64,Vpre::Float64,dt::Float64)
    sinf    = boltz(Vpre,-35.,-5.)
    syn.s   = sinf + (syn.s - sinf)*exp(-dt/(100.*(1-sinf)))
    return Void()
end

function getConductance(syn::Chol)
    syn.s * (Vpost - syn.E)
end
