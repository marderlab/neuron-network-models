


include("neuron.jl")

cells       = Array{Cell,1}()
synapses    = Array{Synapse,1}()

AB          = Cell()
add!(AB,NaV(1830.))     # ḡ in μS/mm^2
add!(AB,CaT(23.))
add!(AB,CaS(27.))
add!(AB,A(246.))
add!(AB,KCa(980.))
add!(AB,Kd(610.))
add!(AB,H(10.))
add!(AB,Leak(.99))
push!(cells,AB)

V, Ca = integrate(cells, synapses, 50e-3, 5e3);

plot(V)
