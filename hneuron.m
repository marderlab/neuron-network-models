classdef hneuron < neuron

    properties
        tau_g = 100; % ms
        tau_mi = [666.7, 55555.6, 45454.5, 5000.0, 1250.0, 2000.0, 125000.0];
        target_Ca = 7.0; % uM
        mRNA = zeros(1,7);
    end

    methods
        % abuse the constructor function to change some things about this object
        function self = hneuron()
            % overwrite available imlementaitons
            self.available_implementations = {'SGS/Julia'};
            self.implementation = 'SGS/Julia';


            self.resetConductances();

            self.t_end = 20e3;

        end % end constructor 

        function resetConductances(self)
            for i = 1:length(self.parameter_names)
                if any(strfind(self.parameter_names{i},'g_'))
                    self.parameters(i) = rand*.1;
                end
            end
        end

        function [V, Ca, N] = integrate(self)
            % assemble parameters
            p = self.p(:)'; %  exponents of m
            max_conductances = self.parameters(~cellfun(@isempty,cellfun(@(x) strfind(x,'g_'),self.parameter_names,'UniformOutput',false)));
            max_conductances = max_conductances(:);
            reversal_potentials = self.reversal_potentials(:)';
            for i = 1:length(self.parameter_names)
                % assignin('caller', self.legal_parameter_names{i}, self.parameters(i))
                feval(@()assignin('caller',self.legal_parameter_names{i}, self.parameters(i)));
            end

            neuron_parameters = [C_m A f Ca_out Ca_in tau_Ca self.t_end self.dt];

            % don't forget the homeostatic parameters!
            initial_conditions = [self.initial_conditions'; self.mRNA';max_conductances];


            % run using Julia
            N = jl.call('intHomeoNeuron7',neuron_parameters,self.reversal_potentials', initial_conditions);

            % export
            V = N(:,12)';
            Ca = N(:,13)';

            % update initial conditions 
            self.initial_conditions = N(end,1:13);
            self.mRNA = N(end,14:20);

            % update conductances
            parameter_idx = find((~cellfun(@isempty,cellfun(@(x) strfind(x,'g_'),self.parameter_names,'UniformOutput',false))));
            parameter_idx(end) = []; % we're not updating leak
            self.parameters(parameter_idx) =  N(end,21:end);

        
        end % end integrate



        function recompile(self,force)
            if ~exist('force','var')
                force = true;
            end


            % add my Julia code
            jl.include('param_types.jl') 
            jl.include('liu_gating_functions.jl') 
            jl.include('homeostaticNeuron.jl') 
            jl.include('neuronMex.jl')

        end % end recompile 

        function manipulate(self)
            % check if a manipulate control window is already open. otherwise, create it
            make_gui = true;
            if isfield(self.handles,'manipulate_control')
                if isvalid(self.handles.manipulate_control)
                    make_gui = false;
                end
            end

            if make_gui
                Height = 900;
                self.handles.manipulate_control = figure('position',[1000 250 1400 Height], 'Toolbar','none','Menubar','none','NumberTitle','off','IntegerHandle','off','CloseRequestFcn',@self.quitManipulateCallback,'Name',['manipulate[neuron+homeostatic control]']);

                % add an axes to show the plots
                self.handles.ax = axes;
                self.handles.ax.Position = [.05 .05 .67 .4];
                xlabel(self.handles.ax,'Time (s)');
                ylabel(self.handles.ax,'V_{m} (mV)');
                self.handles.V_trace = plot(self.handles.ax,NaN,NaN,'k','LineWidth',1.5);
                self.handles.ax.YLim = [-70 70];

                self.handles.ax2 = axes;
                self.handles.ax2.Position = [.05 .5 .67 .4];
                self.handles.C_trace = plot(self.handles.ax2,NaN,NaN,'k','LineWidth',1.5);

                % draw for the first time
                % draw for the first time
                f = self.parameter_names;
                pvec = self.parameters;

                % make sure the bounds are OK
                checkBounds(self);

                % remember which of the parameters correspond to the conductances 
                parameter_idx = find((~cellfun(@isempty,cellfun(@(x) strfind(x,'g_'),self.parameter_names,'UniformOutput',false))));
                control_handles_idx = [];

                Height = 830;
                nspacing = 58.5;
                for i = 1:length(f)
                    self.handles.control(i) = uicontrol(self.handles.manipulate_control,'Position',[1070 Height-i*nspacing 230 20],'Style', 'slider','FontSize',12,'Callback',@self.sliderCallback,'Min',self.lb(i),'Max',self.ub(i),'Value',pvec(i));
       
                    try    % R2013b and older
                       addlistener(self.handles.control(i),'ActionEvent',@self.sliderCallback);
                    catch  % R2014a and newer
                       addlistener(self.handles.control(i),'ContinuousValueChange',@self.sliderCallback);
                    end

                    % hat tip: http://undocumentedmatlab.com/blog/continuous-slider-callback
                    thisstring = [f{i} '= ',mat2str(self.parameters(i))];
                        
                    if any(strfind(self.parameter_names{i},'g_')) 
                        control_handles_idx = [control_handles_idx i];
                    end

                    self.handles.controllabel(i) = text(self.handles.ax,1150,70,thisstring,'FontSize',20);
                    self.handles.lbcontrol(i) = uicontrol(self.handles.manipulate_control,'Position',[1305 Height-i*nspacing+3 40 20],'style','edit','String',mat2str(self.lb(i)),'Callback',@self.resetSliderBounds);
                    self.handles.ubcontrol(i) = uicontrol(self.handles.manipulate_control,'Position',[1350 Height-i*nspacing+3 40 20],'style','edit','String',mat2str(self.ub(i)),'Callback',@self.resetSliderBounds);


                end
                for i = 1:length(f)
                    self.handles.controllabel(i).Position = [1150 230-(i-1)*23];
                end

                % while the window is valid, simulate in 1-second blocks and show it
                window_size = 1000;
                self.handles.ax.XLim = [0 window_size];
                self.handles.ax2.XLim = [0 window_size];
                self.handles.ax2.YLim = [0 self.target_Ca*3];
                dt = 100e-3;
                self.dt = dt;

                buffer_size = 10; 
                buffer_chunk_size = floor(buffer_size/dt);
                self.t_end = buffer_size;

                T = (dt:dt:window_size);
                V = NaN*(dt:dt:window_size);
                C = NaN*(dt:dt:window_size);

                % make sure it runs realtime 
                total_time = 0;

                while isfield(self.handles,'ax')
                    if isvalid(self.handles.ax)
                        tic
                        [this_V, this_C] = self.integrate;

                        % throw out the first bit 
                        V(1:buffer_chunk_size) = [];
                        C(1:buffer_chunk_size) = [];
                        
                        % append the new data to the end
                        V = [V this_V];
                        C = [C this_C];
        
                        self.handles.V_trace.XData = T;
                        self.handles.V_trace.YData = V;

                        self.handles.C_trace.XData = T;
                        self.handles.C_trace.YData = C;

                        drawnow limitrate
                        t = toc;
                        pause_time = (buffer_size/1e3) - t; 
                        if pause_time > 0
                            pause(pause_time*3)
                        end
                        total_time = total_time + self.t_end*1e-3;
                        %disp(total_time)

                        % update conductances 
                        for i = 1:length(control_handles_idx)
                            a = parameter_idx(i);
                            b = control_handles_idx(i);
                            self.handles.control(b).Value = self.parameters(a);
                            new_string = [self.parameter_names{a} ' = ' oval(self.parameters(a))];
                            self.handles.controllabel(b).String = new_string;
                        end
                    end
                end


            end % end if make-gui
        end % end manipulate 


    end

end